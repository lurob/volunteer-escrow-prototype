<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(){
        return view('pages.auth.login.login');
    }

    public function store(LoginRequest $loginRequest){
        $validatedRequest = $loginRequest->validated();
        $email = $validatedRequest['email'];

        $locatedUser = $this->userRepository->findByEmail($email);

        if (!is_null($locatedUser)){
            $savedHash = $locatedUser->password;
            $passwordCheck = Hash::check($validatedRequest['password'], $savedHash);
            if ($passwordCheck){
                Auth::login($locatedUser);
            }
        }

        // Raise error here
    }
}
