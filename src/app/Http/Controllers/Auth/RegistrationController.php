<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(){
        return view('pages.auth.register.register');
    }

    public function store(RegistrationRequest $registrationRequest){
        $validatedRequest = $registrationRequest->validated();
        $registeredUser = $this->userRepository->create(
            $validatedRequest['email'],
            $validatedRequest['fname'],
            $validatedRequest['lname'],
            $validatedRequest['password']
        );
        Auth::login($registeredUser);

        return redirect('https://connect.stripe.com/express/oauth/authorize?client_id=ca_H1XhDNnf1jkjAcXOBHhEqNpPLtUwuVwI&state='. csrf_token() .'&stripe_user[email]='.$validatedRequest['email']);
    }
}
