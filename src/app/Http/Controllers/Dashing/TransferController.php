<?php

namespace App\Http\Controllers\Dashing;

use App\Http\Controllers\Controller;
use App\Models\Stripe\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \Stripe\Stripe::setApiKey('sk_test_MCI5KycGY7r879aa2yq9toiq00sdg47a7r');
        $balance = \Stripe\Balance::retrieve([
            'stripe_account' => Account::where('user_id', Auth::user()->id)->first()->stripe_user_id
        ]);
        return view('pages.dashing.transfers.index', [
            'balance' => head($balance->toArray()['available'])['amount']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Stripe\Stripe::setApiKey('sk_test_MCI5KycGY7r879aa2yq9toiq00sdg47a7r');
        $balance = \Stripe\Balance::retrieve([
            'stripe_account' => Account::where('user_id', Auth::user()->id)->first()->stripe_user_id
        ]);
        return view('pages.dashing.transfers.create', [
            'balance' => head($balance->toArray()['available'])['amount']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        \Stripe\Stripe::setApiKey('sk_test_MCI5KycGY7r879aa2yq9toiq00sdg47a7r');
        $balance = \Stripe\Balance::retrieve([
            'stripe_account' => Account::where('user_id', Auth::user()->id)->first()->stripe_user_id
        ]);
        return view('pages.dashing.transfers.show', [
            'balance' => head($balance->toArray()['available'])['amount']
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
