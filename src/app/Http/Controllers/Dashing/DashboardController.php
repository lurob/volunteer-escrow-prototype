<?php

namespace App\Http\Controllers\Dashing;

use App\Http\Controllers\Controller;
use App\Models\Stripe\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        \Stripe\Stripe::setApiKey('sk_test_MCI5KycGY7r879aa2yq9toiq00sdg47a7r');
        $balance = \Stripe\Balance::retrieve([
            'stripe_account' => Account::where('user_id', Auth::user()->id)->first()->stripe_user_id
        ]);
        return view('pages.dashing.dashboard.index', [
            'balance' => head($balance->toArray()['available'])['amount']
        ]);
    }
}
