<?php


namespace App\Repositories\Interfaces;


interface UserRepositoryInterface
{
    public function all();

    public function findById($id);

    public function create($email, $first_name, $last_name, $password);

    public function findByEmail($email);
}
