<?php


namespace App\Repositories;


use App\Models\Auth\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{

    public function all()
    {
        return User::all();
    }

    public function findById($id, $explicit=true)
    {
        if ($explicit){
            return User::where('id', $id)->first();
        } else {
            return User::where('id', $id)->get();
        }
    }

    public function create($email, $first_name, $last_name, $password)
    {
        return User::create([
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'password' => Hash::make($password)
        ]);
    }

    public function findByEmail($email, $explicit=true)
    {
        if ($explicit){
            return User::where('email', $email)->first();
        } else {
            return User::where('email', $email)->get();
        }
    }
}
