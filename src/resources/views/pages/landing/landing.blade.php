@extends('layouts.base')

@section('title', 'Netcompany Volunteer Escrow Service')

@section('content')
    @include('pages.landing.sections.hero')
    @include('pages.landing.sections.features')
    @include('pages.landing.sections.footer')
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.1/dist/alpine.js" defer></script>
@endpush
