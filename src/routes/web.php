<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.landing.landing');
});


Route::get('/register', 'Auth\RegistrationController@index')->name('register.index');
Route::post('/register', 'Auth\RegistrationController@store')->name('register.store');

Route::get('/login', 'Auth\LoginController@index')->name('login.index');
Route::post('/login', 'Auth\LoginController@store')->name('login.store');

Route::get('/dashboard', 'Dashing\DashboardController@index')->name('dashing.dashboard.index')->middleware(['auth', 'connected']);

Route::resource('/transfers', 'Dashing\TransferController')->middleware(['auth', 'connected']);

Route::get('/oauth/redirect', 'Stripe\OAuthRedirectController@onboardingResponse');

Route::get('/logout', function (){ Auth::logout(); });
